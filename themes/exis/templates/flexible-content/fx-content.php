<?php
defined( 'ABSPATH' ) || exit;

if( have_rows('fx_add_content') ):

    while ( have_rows('fx_add_content') ) : the_row();

        get_template_part('templates/flexible-content/fx-rows');

    endwhile;

else :
    // no flexible content

endif;

?>
