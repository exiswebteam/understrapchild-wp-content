<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/*====================================================================
   Includes - Set up theme
====================================================================*/
$child_theme_includes = array(
    'inc/acf/acf-setup.php',
    'inc/theme-setup.php'
);

//Inclusions
foreach ($child_theme_includes as $file)
{
    if (!$filepath = locate_template($file))
    {
        trigger_error(sprintf(__('Error locating file for inclusion'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);



/*====================================================================
   SVG - support
====================================================================*/
function add_file_types_to_uploads($file_types)
{
  $new_filetypes = array();
  $new_filetypes['svg'] = 'image/svg+xml';
  $file_types = array_merge($file_types, $new_filetypes );
  return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');

function custom_admin_head()
{
  $css = '';
  $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';
  echo '<style type="text/css">'.$css.'</style>';
}
add_action('admin_head', 'custom_admin_head');



/*====================================================================
  Body classes
====================================================================*/
add_filter('body_class', 'global_body_classes');

function global_body_classes($classes)
{
  //defaults
  $fx_content_active;

  //Flexible Content - Content
  $fx_content_active = ( have_rows('fx_add_content') ) ? 'fx-content-active':'fx-content-empty';


  //Generate classes
  $classes[] = $fx_content_active;

  return $classes;
}
