<?php
/**
 * Template Name: Home
 *
 * @package exis
 */

defined( 'ABSPATH' ) || exit;
get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<div class="wrapper" id="single-wrapper">
    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
        <div class="row">
            <main class="site-main" id="main">
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php
                    the_content();

                    /* Flexible content */
                    get_template_part('templates/flexible-content/fx-content');
                    //understrap_post_nav();
                endwhile; ?>
            </main><!-- #main -->
        </div><!-- .row -->
    </div><!-- #content -->
</div><!-- #single-wrapper -->
<?php get_footer(); ?>
