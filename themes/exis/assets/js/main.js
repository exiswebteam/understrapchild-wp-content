/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
(function($)
{
  var EXIS_JS = {
    // All pages
    'common': {
      init: function()
      {

      },
      finalize: function()
      {
        //vars
        var $body = $('body');
        var $window = $(window);

        /* Wait window to fully load
        ------------------------------------------------*/
        $window.load(function()
        {
          //Class for global animations
          $body.addClass('site-loaded');
        }); /**** Window fully loaded ****/
      }
    },
    'home': {
      init: function()
      {

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = EXIS_JS;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);



  /**************************************************************************************************/
  /************************************   jQuery FUNCTIONS  *****************************************/
  /**************************************************************************************************/




})(jQuery); /******** Fully reference jQuery after this point. *************/
