# EXIS

This is the repo for EXIS's project wp-content base file.

## Getting Started (For DEVs) - Local Machine tools

The following steps will help get your project setup using MAMP, XAMP, Node.js, Composer, Wordmove, gulp.

### Setting up your local environment

The following steps outline the process for setting up your local environment

#### First Local environment using XAMP, MAMP or whatever (if not skip ahead to Build Site assets)

The following steps will get Variable Vagrant Vagrants (VVV), Variable VVV (vv), and VVV dashboard running on your computer.

##### Setting up MAMP or XAMP, flywheel, VAGRANT or whatever you are using

Download and follow the instructions under:

XAMP: https://www.apachefriends.org/download.html

MAMP: https://www.mamp.info/en/downloads/

Use latest php version. Config or settings.

#### Install Composer to your machine root dir

https://getcomposer.org/download/

USE: composer.json to add or remove plugins using: https://wpackagist.org/

OR get plugins and uploads from the server. (staging or production)

#### Install git flow to your root dir

https://danielkummer.github.io/git-flow-cheatsheet/

#### Install NODE.js to your root dir

https://nodejs.org/en/

### SSH access to the server/plesk

We need to ensure you have SSH access to the server. Research and find out more info.

### Setting up your staging environment - PLESK

If you are working on an existing project you can skip ahead.

Login to PLESK and create your project. For example: myproject.exis.website

Install wordpress, db user, db name and etc.

In a web browser go to myproject.exis.website

This will prompt the WordPress setup process. Enter in the sitename and your database information for your staging environment.

Once this step is complete navigate to the Plugins area and activate the WP Migrate DB and WP Migrate DB plugins. After these have been activated go to Tools/WP Migrate and click on the Settings tab. On the settings page enter in the License key in the "Your License" box. The License key can be found in OneDrive.

At this point you will want to activate any other plugins that you intend to use as well. Most importantly you will need to activate Migrate DB Pro and Advanced Custom Fields Pro. Advanced Custom Fields Pro and Migrate DB Pro will need to be registered. Registration keys for each of these plugins are located in OneDrive shared folder

You may also want to activate some of the other plugins included in this package. Usually are:

* WPS Hide login
* Yoast SEO plugin
* Contact form 7
* Classic editor
* Duplicate post
* SVG support
* Autoconvert Greeklish Permalinks

Next, navigate to Appearance/Themes and activate the Whisk theme.

At this point you are ready to start adding your pages and menu items

#### Add repo to slack project channel

Type inside the  and follow the instructions: /bitbucket connect git@bitbucket.org:exiswebteam/wp-content.git

## Build your project assets on your local machine

#### Wordpress and repo installation

Navigate to your http docs folder on XAMP/MAMP and create your project folder. For example: localhost/PROJECTNAME/

Install/paste wordpress files in to your PROJECTNAME folder except wp-content.

In terminal navigate to your PROJECTNAME folder under XAMP/MAMP and clone the project repository:

cd C://XAMP/httdocs/PROJECTNAME/ and git clone https://cmartexis@bitbucket.org/exiswebteam/wp-content.git

Navigate to your repo and rename folder from projectname-wp-content to wp-content (C://XAMP/httdocs/PROJECTNAME/) and run:

```
 git flow init
```

Press enter to all branches and last when you asked for release version type:

```
v
```

and press enter. Its all set now you are switched to develop branch. Pull down from develop: git pull origin develop

Next inside wp-content run: composer update. You will need to use composer to install the required plugins. If you are not using composer.json you can also download the plugins from staging or production or install them manually.

Next, in terminal Navigate to the theme folder:

```
cd themes/exis
```

Install NPM depedencies and run:
```
npm install
gulp --production
```

Go to your browser and go to http://localhost/YOURPROJECTNAMEFOLDER/

Follow the instructions to install wordpress. (First make sure you have create a database http://localhost/phpMyAdmin) Ideally your first login before syncing it will have a simple user and password. We just need to login and activate two plugins. (after syncing with staging using WP migrade DB pro it will overwrite the local db with the users from staging)

Login to your local and go to plugins. Activate WP MIGRADE DB PRO and enter liscence key.

Go to staging and copy the key from WP MIGRADE DB PRO settings. (make sure pull option is ON/Green)

Go to local back-end under Tools->WP Migrade DB and click PULL. Paste key and click on Media Files to get all media/uploads folder.

You local is now synced with staging

Go to Custom-fields on your local back-end and Check for Sync Available. !IMPORTANT: Always check for custom fields sync available option when using wp-migrade or pull down from repo. We are using acf-pro json folder inside the theme and we exclude Custom Fields Pro menu item from staging and production.

NOTE:

Watch out for .htaccess and update permalinks after installation. You can also use the code below as reference:

```
# BEGIN WordPress
# The directives (lines) between `BEGIN WordPress` and `END WordPress` are
# dynamically generated, and should only be modified via WordPress filters.
# Any changes to the directives between these markers will be overwritten.
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /kourtidis/
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /kourtidis/index.php [L]
</IfModule>
# END WordPress
```



#### Setting up your Movefile.yaml and pushing your initial files to staging or production

This repo includes the Movefile.yaml file required to run Wordmove. This file needs to be updated with your project specific settings

Open the Movefile.yaml file in wp-content/themes/whisk and change the references to whisk to the name of your theme.

EG for your local environment settings
```
local:
  vhost: "http://localhost:8888/YOURPROJECTNAMEFOLDER/"
  WordPress_path: "/Users/User/htdocs/PROJECTNAME/" # use an absolute path here
```

Do the same for your staging environment. Keep in mind the name of the domain and file name you setup for your staging environment within Inmotion.

```
staging:
	  vhost: "https://kourtidis.exis.website"
	  WordPress_path: "/var/www/vhosts/exis.website/kourtidis.exis.website" # use an absolute path here
```

In Terminal navigate to wp-content/themes/whisk and run:

```
wordmove push -e staging -wupt
```

#### Updating plugins

Plugin updates are done using wp-cli. First you will need to setup wp-cli aliases for your machine.

If you have not yet installed wp-cli you can do so by running:

```
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
```
You can now test that wp-cli was installed correctly by running:
```
wp --info
```
Next you will need to setup the aliases for your project:

WP-CLI aliases allow you to ssh into your dev, staging and production environments and control the wp-cli from the root of your local WordPress install. To enable aliases add your environments/paths to the wp-cli.yml file in the WordPress root folder of the project(The file already exists) in the following format:

```
@mycoolalias:
  ssh: user@host/path/to/WordPress
```

For staging:

```
@staging:
  ssh: gbenec@exis.com.gr/public_html/projec
```

NOTE: The wp-cli.yaml file is created automatically by VVV however this file is not included in the project repo. The above steps will need to be repeated on any development machines you are planning to use the aliases on. Also please ensure that you have an SSH key registered with the host and that the key has been added to your SSH agent.

Now you can update your plugins using the wp-cli aliases, EG for Development you can run:

```
wp @dev plugin update --all
```

or
```
wp @staging plugin update --all
```

### Advanced Custom Fields and Flexible Content

The theme relies heavily on Advanced Custom Fields. In particular it is set up to use Flexible Content. Flexible content layouts can be created within the WordPress admin area. Once they have been created they can be cloned so that they can be used in other areas. The markup for these elements should be included in whisk/templates/flexible-content/elements and the link to this template should be added to the whisk/templates/flexible-content/fx-layouts.php file.

When publishing a new page you will be presented with three choices for page layout. Each page layout will present tabs with different options. The Section feature comes populated with a Custom Section however this should only be used for simple content. For highly customized site specific content additional Custom Sections can be created and added to whisk/templates/flexible-content/fx-layouts.php

### Theme settings

Theme settings are stored in the Theme Settings area in the sidebar of the WordPress admin. Setting included here include the Website logo, a text area to include copyright info for the footer, Blog settings (for archives and single posts), Social Media settings and Contact Info.

### Preformatted Styles

The WYSIWYG editor in the whisk theme is setup with a formats area so that text can be given classes without having to edit html to view the formats you must have the WYSIWYG editor fully expanded. Here you will be presented with a number of text, and button styles as well as some visibility classes.


## Working with the EXIS Theme

Documentation speicifically related to working with the understrap custom theme.

### Media assets

As much possible all assets should be uploaded and referenced from  the WP Media Library. You should always give the end user the ability to easily customize any media assets being used with theme. Either use the native featured image option or ACF image option to allow users to choose the media output in the WP Admin area. You can also use a WYSIWYG editor but ACF fields are preferred.

For certain cases where the media is likely to be static for the life of the website or where changing the media could brake the layout or overall look and feel of the site, you may want to use the Whisk assets folder to store your media to be used directly in the source files.

Images are stored in the `assets/images` folder and compiled with a gulp task to the `dist/images` folder. For production always use the compiled versions.

If you’re in a stylesheet and you’d like to reference an image,
assets/images/logo.svg, you would write:
```
.brand {
  background: url(../images/logo.png);
}
```
If you’re in a template and you’d like to reference the same image,
you would write:
```
<img src="<?= get_template_directory_uri() . '/dist/images/
logo.png'; ?>">
```

SVG's are stored in the `assets/svg` folder and compiled to the `dist/svg` folder. Unlike image assets, you cannot nest SVG file inside of folders such as "icon" or "background". All SVG files *must be placed directly* inside the `assets/svg` folder. For separation of files please prefix the SVG filename with its relevance. eg: `icons/filename.svg` would become `icon-filename.svg`.

All SVG optimisation is handled by the gulp task, however with SVG, the exporting from the graphics program must set the "fit artboard to artwork bounds". Otherwise you will be left with unwanted whitespace. Please see [Tips for Creating and Exporting Better SVGs for the Web](https://www.sarasoueidan.com/blog/svg-tips-for-designers/) for a guide to exporting SVGs.

Using SVG is done with inline with PHP. You can inject the code using `file_get_contents('dist/filename.svg');` or by using the convenient helper function, for example `<?= Extras\include_svg(filename); ?>`.

The SVG will have a classname added to the wrapping svg tag, the classname will be the `filename` and prefixed by `svg-`. To style the svg use this classname in your stylesheet. eg: `<svg class="svg-filename">` can be targeted by using `.svg-filename` in the stylesheet.

### Javascript / jQuery

The theme uses a namespaced main.js file that allows you to call javascript/jQuery functions on specific pages or on all pages. For more info on this please see main.js

Whenever possible add your jQuery/javascript functions to the namespaced areas in main.js and add your custom scripts in separate files that are called by manifest.json.
